import React from "react";
import Input from "./container/Input/Input";
import {CssBaseline} from "@material-ui/core";

const App = () => (
    <>
      <CssBaseline/>
      <Input/>
    </>
);

export default App;
