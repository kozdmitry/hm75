import React, {useState} from 'react';
import {Container, CssBaseline, Grid, TextField, Toolbar} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {changeInput, postDecodeMessage, postEncoded} from "../../store/actions";
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';

const Input = () => {
    const dispatch = useDispatch();
    const state = useSelector(state => state);

    console.log(state);

    const post = () => {
        dispatch(postEncoded(state))
    };

    const postDecodeHandler = () => {
        dispatch(postDecodeMessage(state))
    }

    const changeInfo = event => {
        dispatch(changeInput(event.target))
    };

    return (
        <>
            <CssBaseline/>
            <Container>
                <Toolbar/>
                <Grid container direction="column" spacing={2}>
                    <Grid item>
                        <TextField
                            onChange={changeInfo}
                            name="encoded"
                            label="Encoded message"
                            multiline
                            rows={4}
                            variant="outlined"
                            value={state.encoded}
                        >
                        </TextField>
                    </Grid>
                    <Grid item>
                        <TextField
                            onChange={changeInfo}
                            name="password"
                            label="Password"
                            type="password"
                            autoComplete="current-password"
                            variant="outlined"
                            value={state.password}
                        />
                        <Grid item>
                            <button onClick={post}><KeyboardArrowDownIcon/></button>
                            <button onClick={postDecodeHandler}><KeyboardArrowUpIcon/></button>
                        </Grid>
                    </Grid>
                    <Grid item>
                        <TextField
                            onChange={changeInfo}
                            label="Decoded message"
                            name="decoded"
                            multiline
                            rows={4}
                            variant="outlined"
                            value={state.decoded}
                        >
                        </TextField>
                    </Grid>
                </Grid>
            </Container>
        </>
    );
};

export default Input;